// const axios = require('axios')
// const url = 'http://checkip.amazonaws.com/';
const AWS = require('aws-sdk');

AWS.config.update({
  region: 'eu-west-1'
});

const ssm = new AWS.SSM();
let response;

/**
 *
 * Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format
 * @param {Object} event - API Gateway Lambda Proxy Input Format
 *
 * Context doc: https://docs.aws.amazon.com/lambda/latest/dg/nodejs-prog-model-context.html 
 * @param {Object} context
 *
 * Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
 * @returns {Object} object - API Gateway Lambda Proxy Output Format
 * 
 */
const lambdaHandler = async (event, context) => {
    
    const data = await ssm.getParameters({
        Names: [`/dev/test1`]
    }).promise();
    
    console.log(`value of /dev/test1 environment variable is ${data.Parameters[0].Value} (ssm)`);
    
    var secret = await getSecret('test-sm')
    console.log(`value of test-sm environment variable is ${secret} (parameter-store)`)
    
    try {
        // const ret = await axios(url);
        response = {
            'statusCode': 200,
            'body': JSON.stringify({
                message: 'hello world',
                // location: ret.data.trim()
            })
        };
    } catch (err) {
        console.log(err);
        return err;
    }

    return response;
};

async function getSecret(secretName) {
    // Load the AWS SDK
    var AWS = require('aws-sdk'),
        region = "eu-west-1",
        secretName = secretName,
        secret,
        decodedBinarySecret;

    // Create a Secrets Manager client
    var client = new AWS.SecretsManager({
        region: region
    });

    return new Promise((resolve,reject)=>{
        client.getSecretValue({SecretId: secretName}, function(err, data) {

            if (err) {
                reject(err);
            }
            else {

                if ('SecretString' in data) {
                    resolve(data.SecretString);
                } else {
                    let buff = new Buffer(data.SecretBinary, 'base64');
                    resolve(buff.toString('ascii'));
                }
            }
        });
    });
}

exports.lambdaHandler = lambdaHandler;

